﻿using log4net;
using System.Web.Mvc;

namespace AppExamenParcial.Filtros
{
    public class LogAuditoriaAttribute : ActionFilterAttribute
    {
        private static ILog Log { get; set; }

        public LogAuditoriaAttribute()
        {
            Log = LogManager
                .GetLogger("test");
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
             => LogMensaje(filterContext, "OnActionExecuted");

        public override void OnResultExecuted(ResultExecutedContext filterContext)
            => LogMensaje(filterContext, "OnResultExecuted");

        private void LogMensaje(ControllerContext filterContext, string metodo)
        {
            var controller = filterContext.RouteData.Values["controller"];
            var action = filterContext.RouteData.Values["action"];
            var usuario = filterContext.HttpContext.User;
            Log.Info($"En el {metodo} del {controller}.{action}");
        }
    }

}