﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenParcial.Modelo
{
    [Table("Company")]
    public partial class Company
    {
        public int Id { get; set; }
        public string Ruc { get; set; }
        
        public string RazonSocial { get; set; }
        
        public string Direccion { get; set; }
        public string Departamento { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
        public string Rubro{ get; set; }


    }
}

