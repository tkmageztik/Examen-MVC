﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenParcial.Modelo
{
    [Table("Bill")]
    public partial class Bill
    {
        public int Id { get; set; }
        public string Serie { get; set; }
        public string Numero { get; set; }

        public string FecEmi { get; set; }

        public string FecVen{ get; set; }
        public string FecCob{ get; set; }


        public virtual Company Company { get; set; }

        public string TotalFactura { get; set; }
        public string TotalImp{ get; set; }
//        public string Fac{ get; set; } imagen factura escaneada.
    }
}
