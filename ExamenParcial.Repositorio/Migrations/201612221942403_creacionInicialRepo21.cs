namespace ExamenParcial.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class creacionInicialRepo21 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Bill",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Serie = c.String(),
                        Numero = c.String(),
                        FecEmi = c.String(),
                        FecVen = c.String(),
                        FecCob = c.String(),
                        TotalFactura = c.String(),
                        TotalImp = c.String(),
                        Company_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Company", t => t.Company_Id)
                .Index(t => t.Company_Id);
            
            CreateTable(
                "dbo.Company",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Ruc = c.String(),
                        RazonSocial = c.String(),
                        Direccion = c.String(),
                        Departamento = c.String(),
                        Provincia = c.String(),
                        Distrito = c.String(),
                        Rubro = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Bill", "Company_Id", "dbo.Company");
            DropIndex("dbo.Bill", new[] { "Company_Id" });
            DropTable("dbo.Company");
            DropTable("dbo.Bill");
        }
    }
}
