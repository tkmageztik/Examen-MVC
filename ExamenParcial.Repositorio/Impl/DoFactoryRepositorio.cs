﻿using ExamenParcial.Modelo;
using System.Data.Entity;
using System;

namespace AwSales.Repositorio.Impl
{
    public class HumanResourcesRepositorio : IDisposable
    {
        private readonly DbContext bd;

        private readonly RepositorioGenerico<Customer> _clientes;
        private readonly RepositorioGenerico<Order> _ordenes;
        private readonly RepositorioGenerico<OrderItem> _itemsOrden;
        private readonly RepositorioGenerico<Product> _productos;
        private readonly RepositorioGenerico<Supplier> _proveedores;

        public HumanResourcesRepositorio(DbContext bd)
        {
            this.bd = bd;
            _clientes = new RepositorioGenerico<Customer>(bd);
            _ordenes = new RepositorioGenerico<Order>(bd);
            _itemsOrden = new RepositorioGenerico<OrderItem>(bd);
            _productos = new RepositorioGenerico<Product>(bd);
            _proveedores = new RepositorioGenerico<Supplier>(bd);
        }

        public RepositorioGenerico<Customer> Departamentos { get { return _clientes; } }
        public RepositorioGenerico<Order> Empleados { get { return _ordenes; } }
        public RepositorioGenerico<OrderItem> Escalafon { get { return _itemsOrden; } }
        public RepositorioGenerico<Product> PagosEmpleado { get { return _productos; } }
        public RepositorioGenerico<Supplier> Candidatos { get { return _proveedores; } }

        public void Commit()
        {
            bd.SaveChanges();
        }

        public void Dispose()
        {
            bd.Dispose();
        }
    }
}