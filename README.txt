1. Ejecutar el script dofactory para crear la bd.

2. Para crear la bd de seguridad ACTIVAR MIGRATIONS > ENABLE MIGRATIONS

3. add-migration creacionInicial

4. update-database -verbose (ultimo comando si se quieren ver las sentencias sql).

5. crear carpeta C:\Log_parcial\WebDeveloper.txt

* REALIZADO PARA QUE FUNCIONE LOG4NET:


<section name="log4net" type="log4net.Config.Log4NetConfigurationSectionHandler, log4net" />  

dentro de tag <configSections> en webconfig

<log4net debug="true">
    <appender name="RollingLogFileAppender" type="log4net.Appender.RollingFileAppender">
      <file value="C:\Logs\WebDeveloper.txt" />
      <appendToFile value="true" />
      <rollingStyle value="Size" />
      <maxSizeRollBackups value="10" />
      <maximumFileSize value="100KB" />
      <staticLogFileName value="true" />
      <layout type="log4net.Layout.PatternLayout">
        <conversionPattern value="%-5p %d %5rms %-22.22c{1} %-18.18M - %m%n" />
      </layout>
    </appender>
    <root>
      <level value="DEBUG" />
      <appender-ref ref="RollingLogFileAppender" />
    </root>
  </log4net>

en webconfig.

PDTA: El OnAuthorization me estaba dando un bucle por lo que la validación
de operador solo funciona una vez que operador este logeado.

